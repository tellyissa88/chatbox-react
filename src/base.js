import Rebase from 're-base'
import firebase from 'firebase/app'
import 'firebase/database'
 const firebaseApp = firebase.initializeApp({
    apiKey: "AIzaSyBoWMDrTgxiGBUMaH-raOdg5nCAJJJkVAU",
    authDomain: "chatbox-app-fc612.firebaseapp.com",
    databaseURL: "https://chatbox-app-fc612.firebaseio.com"
 })

 const base = Rebase.createClass(firebase.database())
  export { firebaseApp }
  export default base