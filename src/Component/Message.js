import React from 'react';

const Message = ({ isUser, pseudo, message }) => {
    if(isUser(pseudo)){
        return (
            <p className="user-message">
                 {pseudo}
                {message}
            </p>
         );
    }else{
        return (
            <p className="user-message-blue">
                 {pseudo}
                {message}
            </p>
         );
    }
    
};

export default Message;